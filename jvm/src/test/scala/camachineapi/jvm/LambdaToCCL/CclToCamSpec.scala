package camachineapi.jvm.LambdaToCCL

import camachineapi.CAMachine.commands.{Car, Cdr}
import camachineapi.CAMachine.{commands, Code}
import camachineapi.jvm.LambdaToCCL.DeBruijnToCcl.snd
import org.specs2.Specification

final class CclToCamSpec extends Specification {
  def is =
    s2"""
      CclToCam should
        $convert_CCL_to_Cam_commands_simple
        $convert_CCL_to_Cam_commands_complex
    """
  private def convert_CCL_to_Cam_commands_simple =
    CclToCam.toCam(
      Dpair(CombCur(Compose(1)), CombCur(snd)),
    ) must beEqualTo(
      List(
        commands.Push,
        commands.Cur(Code(List(Car, Cdr))),
        commands.Swap,
        commands.Cur(Code(List(Cdr))),
        commands.Cons,
        commands.App,
      ),
    )

  private def convert_CCL_to_Cam_commands_complex =
    CclToCam.toCam(
      Dpair(Dpair(Sum(), IntTerm(1)), IntTerm(2)),
    ) must beEqualTo(
      List(
        commands.Push,
        commands.Push,
        commands.Quote("+"),
        commands.Swap,
        commands.Quote("1"),
        commands.Cons,
        commands.App,
        commands.Swap,
        commands.Quote("2"),
        commands.Cons,
        commands.App,
      ),
    )
}

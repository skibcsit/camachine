package camachineapi.jvm.LambdaToCCL

import camachineapi.jvm.LambdaToCCL.DeBruijnToCcl.snd
import org.specs2.Specification

final class DeBruijnToCclSpec extends Specification {
  def is =
    s2"""
      DeBruijnToCcl should
        $convert_to_CCL_term_simple
        $convert_to_CCL_term_complex
    """
  private def convert_to_CCL_term_simple =
    DeBruijnToCcl.toCCL(
      App(
        Lambda(Var(Literal("x")), Var(Index(1, "x"))),
        Lambda(Var(Literal("x")), Var(Index(0, "x"))),
      ),
    ) must beEqualTo(
      Dpair(CombCur(Compose(1)), CombCur(snd)),
    )

  private def convert_to_CCL_term_complex =
    DeBruijnToCcl.toCCL(
      App(App(Sum(), IntTerm(1)), IntTerm(2)),
    ) must beEqualTo(
      Dpair(Dpair(Sum(), IntTerm(1)), IntTerm(2)),
    )
}

package camachineapi.jvm.LambdaToCCL

import camachineapi.jvm.LambdaToCCL.LambdaReduce.beta
import org.specs2.Specification

final class LambdaToDeBruijnSpec extends Specification {
  def is =
    s2"""
      LambdaReduce should
        $substitude_beta_reduction
    """
  private def substitude_beta_reduction =
    beta(
      App(
        Lambda(Var(Index(0, "x")), Var(Literal("x"))),
        Lambda(Var(Index(0, "x")), Var(Literal("x"))),
      ),
    ) must beSome(Var(Literal("x")))

}

package camachineapi.jvm.LambdaToCCL

import scala.util.parsing.combinator.lexical.StdLexical
import scala.util.parsing.combinator.{PackratParsers, RegexParsers}
import scala.util.parsing.input.CharSequenceReader
import fastparse._
import fastparse.NoWhitespace._
import fastparse.internal.Util

class LambdaParser extends RegexParsers with PackratParsers {
  type Tokens = StdLexical
  val lexical = new CustomLambdaLexer
  lexical.delimiters ++= Seq("\\", ".", "(", ")")

  lazy val lambdaExpression: PackratParser[Term] =
    application | otherExpressionTypes
  lazy val otherExpressionTypes: Parser[Term] =
    variable | parens | lambda | builtInOps
  lazy val lambda: PackratParser[Lambda] =
    positioned("\\" ~> variable ~ "." ~ parens ^^ {
      case arg ~ "." ~ body =>
        Lambda(arg, body)
      case _ => ???
    })
  lazy val application: PackratParser[App] =
    positioned(lambdaExpression ~ otherExpressionTypes ^^ { case left ~ right =>
      App(left, right)
    })
  lazy val variable: PackratParser[Var] =
    positioned(
      """[A-Za-z]""".r ^^ ((x: String) => Var(Literal(x.charAt(0).toString))),
    )
  lazy val intValue: PackratParser[IntTerm] = positioned {
    """[1-9]\d*|0""".r ^^ { str =>
      IntTerm(str.toInt)
    }
  }
  lazy val sum: PackratParser[Sum] = positioned("+" ^^ (_ => Sum()))
  lazy val sub: PackratParser[Sub] = positioned("-" ^^ (_ => Sub()))
  lazy val mul: PackratParser[Mult] = positioned("*" ^^ (_ => Mult()))
  lazy val div: PackratParser[Div] = positioned("/" ^^ (_ => Div()))
  lazy val eq: PackratParser[Eq] = positioned("=" ^^ (_ => Eq()))
  lazy val builtInOps: PackratParser[BuiltIn] =
    intValue | sum | sub | mul | div | eq
  lazy val parens: PackratParser[Term] =
    "(" ~> lambdaExpression <~ ")" | lambdaExpression

}

object LambdaParser {

  def comment[_: P] = P("/*" ~/ (!"*/" ~ AnyChar).rep ~/ "*/")

  def newline[_: P] = P("\n" | "\r\n" | "\r" | "\f")

  def whitespace[_: P] = P(" " | "\t" | newline)

  def whitespaceToken[_: P] = P(comment | whitespace.rep(1))

  def ws[_: P] = P(whitespaceToken.rep)

  def digit[_: P] = CharIn("0-9")

  def letter[_: P] = P(SingleChar(CharIn("a-zA-Z")))

  def variable[_: P]: P[Var] =
    P(letter.map(x => Var(Literal(x.toString))))
  def parens[_: P]: P[Term] =
    P("(" ~/ lambdaExpression ~ ")" | lambdaExpression)
  def lambda[_: P]: P[Lambda] = P("\\" ~/ variable ~ "." ~ parens).map {
    case (arg, body) => Lambda(arg, body)
  }
  def otherExpressionTypes[_: P]: P[Term] = P(variable | lambda | parens)
  def application[_: P]: P[App] =
    P(lambdaExpression ~/ otherExpressionTypes).map { case (left, right) =>
      App(left, right)
    }

  def lambdaExpression[_: P]: P[Term] = P(application | otherExpressionTypes)
  def parseLambdaExpression(code: String): Either[String, Term] =
    parse(code, lambdaExpression(_)).fold(
      (stack, index, extra) =>
        Left(s"Failure! at index: $index found: ... ${Util.literalize(
          extra.input.slice(index, index + 15),
        )} expected: ${extra.trace().label}"),
      (v, _) => Right(v),
    )

}

class CustomLambdaLexer extends StdLexical {
  override def letter: Parser[Char] =
    elem(
      "letter",
      c =>
        c.isLetter &&
          c != '(' && c != ')' && c != '\\' && c != '.',
    )
}

object SimpleLambdaParser extends LambdaParser {
  def parse(s: CharSequence): Term =
    parse(new CharSequenceReader(s))

  def parse(input: CharSequenceReader): Term =
    parsePhrase(input) match {
      case Success(t, _) => t
      case NoSuccess(msg, next) =>
        throw new IllegalArgumentException(
          "Could not parse '" + input + "' near '" + next.pos.longString + ": " + msg,
        )
      case Error(msg, next) =>
        throw new IllegalArgumentException(
          "Could not parse '" + input + "' near '" + next.pos.longString + ": " + msg,
        )
      case Failure(msg, next) =>
        throw new IllegalArgumentException(
          "Could not parse '" + input + "' near '" + next.pos.longString + ": " + msg,
        )
    }

  def parsePhrase(input: CharSequenceReader): ParseResult[Term] =
    phrase(lambdaExpression)(input)
}

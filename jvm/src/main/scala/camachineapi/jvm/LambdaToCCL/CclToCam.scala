package camachineapi.jvm.LambdaToCCL

import camachineapi.CAMachine.Code
import camachineapi.CAMachine.commands._

object CclToCam {
  def toCam(term: TermCCL): List[Command] = term match {
    case Fst()        => List(Car)
    case Snd()        => List(Cdr)
    case Compose(n)   => List.fill(n)(Car) :+ Cdr
    case IntTerm(num) => List(Quote(num.toString))
    case Sum()        => List(Quote(Sum().toString))
    case Sub()        => List(Quote(Sub().toString))
    case Mult()       => List(Quote(Mult().toString))
    case Div()        => List(Quote(Div().toString))
    case Eq()         => List(Quote(Eq().toString))
    case CombCur(termCCL) =>
      List(Cur(Code(toCam(termCCL))))
    case Dpair(termCCL1, termCCL2) =>
      ((Push :: toCam(termCCL1)).appended(Swap) ::: toCam(termCCL2))
        .appended(Cons) :+ App
  }
//  def toCam(term: TermCCL): String = term match {
//    case Fst(_, _) => "car"
//    case Snd(_, _) => "cdr"
//    case CombCur(termCCL) =>
//    fs"cur(${toCam(termCCL)})"
//    case Dpair(termCCL1, termCCL2) =>
//    fs"push ${toCam(termCCL1)} swap ${toCam(termCCL2)} cons app"
//  }

}

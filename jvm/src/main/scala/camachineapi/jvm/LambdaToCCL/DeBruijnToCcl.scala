package camachineapi.jvm.LambdaToCCL

object DeBruijnToCcl {

  val fst: Fst = Fst()
  val snd: Snd = Snd()
//  App(Lambda(Var(Index(0,"a")),Var(Index(1,"b"))),Var(Index(1,"x")))
//  CombApp(CombCur(CombApp(Fst(None,None),Snd(None,None)),Snd(None,None)),CombApp(Fst(None,None),Snd(None,None)))
  def toCCL(term: Term): TermCCL = term match {
    case Var(name) =>
      name match {
        case Index(j, _) if j == 0 => snd
        case Index(j, _) if j > 0  => Compose(j)
        case _                     => Compose(1)
      }
    case Lambda(_, body)   => CombCur(toCCL(body))
    case App(term1, term2) => Dpair(toCCL(term1), toCCL(term2))
    case b: BuiltIn        => b

  }

}

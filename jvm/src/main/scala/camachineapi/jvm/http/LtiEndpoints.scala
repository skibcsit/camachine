package camachineapi.jvm.http

import camachineapi.jvm.config.MoodleConfig
import camachineapi.models.{LtiScoreData, LtiTokenResponse}
import camachineapi.{CamApiEndpoints, LtiApiEndpoints}
import cats.effect.{ConcurrentEffect, ContextShift, Sync, Timer}
import cats.implicits.catsSyntaxOptionId
import cats.syntax.either._
import cats.syntax.flatMap._
import cats.syntax.functor._
import com.github.plokhotnyuk.fsi.FastStringInterpolator
import endpoints4s.http4s.server.{Endpoints, JsonEntitiesFromSchemas}
import io.circe.parser
import org.http4s.{HttpRoutes, Status, Uri}
import pdi.jwt._
import sttp.client3._
import sttp.client3.circe._

import java.time.{Instant, LocalDateTime}
import scala.util.Random

final class LtiEndpoints[F[_]: Sync: ContextShift: ConcurrentEffect: Timer](
  moodleConfig: MoodleConfig,
  sttpClient: SttpBackend[F, Any],
) extends Endpoints[F]
    with JsonEntitiesFromSchemas
    with LtiApiEndpoints
    with CamApiEndpoints {
  def found: Status = Status.Found
  val safeChars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwqyx0123456789"
  def safeString(n: Int): String =
    (0 until n).map { _ =>
      safeChars(Random.nextInt(safeChars.length))
    }.mkString

  def endpoints(): HttpRoutes[F] = HttpRoutes.of[F] {
    routesFromEndpoints(
      login.implementedBy { data =>
        val state = safeString(28)
        val nonce = safeString(30)
        Uri
          .fromString(fs"${moodleConfig.url}/mod/lti/auth.php").fold(
            err => err.message.asLeft[String],
            _.withQueryParam("scope", "openid")
              .withQueryParam("response_type", "id_token")
              .withQueryParam("response_mode", "form_post")
              .withQueryParam("prompt", "none")
              .withQueryParam("client_id", data.clientId)
              .withQueryParam("redirect_uri", data.targetLinkUri)
              .withQueryParam("state", state)
              .withQueryParam("nonce", nonce)
              .withQueryParam("login_hint", data.loginHint)
              .withQueryParam(
                "lti_message_hint",
                data.ltiMessageHint,
              ).renderString.asRight[String],
          )
      },
      launch.implementedBy { data =>
        JwtCirce
          .decodeJson(
            data.idToken,
            moodleConfig.pubKey,
            Seq(JwtAlgorithm.RS256),
          ).fold(
            _.getMessage.asLeft[String],
            json => {
              val cursor = json.hcursor
              for {
                id <- cursor
                  .downField(
                    "https://purl.imsglobal.org/spec/lti/claim/context",
                  ).get[Int]("id").left.map(_.message)
                name <- cursor.get[String]("name").left.map(_.message)
                lisString <- cursor
                  .downField(
                    "https://purl.imsglobal.org/spec/lti-bo/claim/basicoutcome",
                  ).get[String]("lis_result_sourcedid").left.map(_.message)
                rawLis = lisString.replace("""\""", "")
                dataParsed <- parser.parse(rawLis).left.map(_.message)
                userId <- dataParsed.hcursor
                  .downField("data")
                  .get[Int]("userid").left.map(_.message)
              } yield s"http://localhost:8080/#$id/$userId/$name"
            },
          )
      },
      submitScore.implementedByEffect { data =>
        val claimJson = JwtClaim(
          expiration = Some(Instant.now.plusSeconds(157784760).getEpochSecond),
          issuedAt = Some(Instant.now.getEpochSecond),
          subject = moodleConfig.clientId.some,
          issuer = moodleConfig.url.renderString.some,
          audience = Set(fs"${moodleConfig.url}/mod/lti/token.php").some,
        )
        val token =
          JwtCirce.encode(claimJson, moodleConfig.secKey, JwtAlgorithm.RS256)
        for {
          respToken <- basicRequest
            .body(
              "grant_type" -> "client_credentials",
              "client_assertion_type" -> "urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
              "client_assertion" -> token,
              "scope" -> "https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/lineitem.readonly https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score",
            )
            .post(
              uri"${moodleConfig.url}/mod/lti/token.php",
            )
            .response(asJson[LtiTokenResponse].getRight)
            .send(sttpClient)
          scoreResult <- basicRequest.auth
            .bearer(respToken.body.accessToken)
            .contentType("application/vnd.ims.lis.v1.score+json")
            .body(
              LtiScoreData(
                "Completed",
                "some comment",
                "FullyGraded",
                60,
                100,
                LocalDateTime.now(),
                data.userId,
              ),
            )
            .post(
              uri"${moodleConfig.url}/mod/lti/services.php/2/lineitems/2/lineitem/scores?type_id=1",
            )
            .response(ignore)
            .send(sttpClient)
        } yield scoreResult.body.asRight[String]
      },
    )
  }
}

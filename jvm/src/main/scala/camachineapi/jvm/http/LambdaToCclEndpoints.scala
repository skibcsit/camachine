package camachineapi.jvm.http

import camachineapi.CamApiEndpoints
import camachineapi.jvm.LambdaToCCL.{
  CclToCam,
  DeBruijnToCcl,
  SimpleLambdaParser,
}
import camachineapi.models.LambdaCode
import cats.effect.Async
import com.github.plokhotnyuk.fsi.FastStringInterpolator
import endpoints4s.http4s.server.{Endpoints, JsonEntitiesFromSchemas}
import org.http4s.HttpRoutes

import scala.util.{Failure, Success, Try}

final class LambdaToCclEndpoints[F[_]](implicit
  F: Async[F],
) extends Endpoints[F]
    with JsonEntitiesFromSchemas
    with CamApiEndpoints {

  def endpoints(): HttpRoutes[F] = HttpRoutes.of[F] {
    routesFromEndpoints(lambda.implementedBy { data =>
      Try(SimpleLambdaParser.parse(data.code)) match {
        case Success(term) =>
          val cclTerm = DeBruijnToCcl
            .toCCL(term)
          LambdaCode(
            fs"${term.toString} -> ${cclTerm.toString} -> ${CclToCam
              .toCam(cclTerm).mkString(" ")}",
          )
        case Failure(ex) => LambdaCode(ex.getMessage)
      }
    })
  }
}

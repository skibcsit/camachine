package camachineapi.jvm.http

import camachineapi.CAMachine.{Compiler, Row}
import camachineapi.CamApiEndpoints
import camachineapi.models.{CamCompilerResult, RecvarsDto, VariableDto}
import cats.effect.Async
import mouse.boolean._
import cats.syntax.option._
import endpoints4s.http4s.server.{Endpoints, JsonEntitiesFromSchemas}
import org.http4s.HttpRoutes

import scala.util.Random

final class CamEndpoints[F[_]](implicit
  F: Async[F],
) extends Endpoints[F]
    with JsonEntitiesFromSchemas
    with CamApiEndpoints {

  def endpoints(): HttpRoutes[F] = HttpRoutes.of[F] {
    routesFromEndpoints(
      cam.implementedBy { data =>
        val compiler = new Compiler
        val memory = compiler.memory
        compiler.compile(data.commands, data.optimisation)
        val result = CamCompilerResult(
          memory.recvars.toList
            .map(recvar =>
              RecvarsDto(recvar.getName, recvar.getValue.toString()),
            ),
          memory.variables.toList
            .map(variable =>
              VariableDto(variable.getName, variable.getValue.toString()),
            ),
          memory.getRows.toList,
          memory.getError,
        )
        result
      },
      camTask.implementedBy { _ =>
        val compiler = new Compiler
        val memory = compiler.memory
        val samples = List(
          "push push recur(push push cdr swap quote(0) cons equal branch(quote(1))(push cdr swap push car cdr swap push cdr swap quote(1) cons minus cons app cons mult)) cons cur(push push cdr swap quote(0) cons equal branch(quote(1))(push cdr swap push car cdr swap push cdr swap quote(1) cons minus cons app cons mult)) cons push cdr swap quote(3) cons app",
          "push cur(cdr) swap push cur(cdr) swap quote(a) cons app cons app",
          "push quote(2) swap quote(3) cons add",
          "push push recur(push push cdr swap quote(0) cons equal branch(quote(0))(push cdr swap push car cdr swap push cdr swap quote(1) cons minus cons app cons add)) cons cur(push push cdr swap quote(0) cons equal branch(quote(0))(push cdr swap push car cdr swap push cdr swap quote(1) cons minus cons app cons add)) cons push cdr swap quote(3) cons app",
          "push push push quote() cons push cur(push push cdr swap quote(0) cons equal branch(quote(1))(push cdr swap push car cdr swap push cdr swap quote(1) cons minus cons app cons mult)) mkloop cons cur(push push cdr swap quote(0) cons equal branch(quote(1))(push cdr swap push car cdr swap push cdr swap quote(1) cons minus cons app cons mult)) cons push cdr swap quote(3) cons app",
          "push cur(quote(1)) swap push cur(push cdr swap cdr cons app) swap cur(push cdr swap cdr cons app) cons freeze app cons app unfreeze",
          "push push quote(+) swap quote(2) cons app swap quote(3) cons app",
        )
        val random = new Random
        compiler.compile(samples(random.nextInt(samples.size)), 0)
        val emptyElemIndex = random.nextInt(memory.getRows.length - 1)
        val result = CamCompilerResult(
          memory.recvars.toList
            .map(recvar =>
              RecvarsDto(recvar.getName, recvar.getValue.toString()),
            ),
          memory.variables.toList
            .map(variable =>
              VariableDto(variable.getName, variable.getValue.toString()),
            ),
          memory.getRows.toList.zipWithIndex.map(p =>
            (p._2 == emptyElemIndex && emptyElemIndex != 0).fold(
              t = Row(
                Option.empty[String],
                Option.empty[String],
                Option.empty[String],
              ),
              f = Row(p._1.term.some, p._1.code.some, p._1.stack.some),
            ),
          ),
          memory.getError,
        )
        result
      },
    )

  }

}

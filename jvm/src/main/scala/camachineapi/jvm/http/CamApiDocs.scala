package camachineapi.jvm.http

import camachineapi.{CamApiEndpoints, LtiApiEndpoints}
import endpoints4s.openapi
import endpoints4s.openapi.model.{Info, OpenApi}

object CamApiDocs
    extends CamApiEndpoints
    with LtiApiEndpoints
    with openapi.Endpoints
    with openapi.JsonEntitiesFromSchemas {

  val api: OpenApi =
    openApi(
      Info(title = "Category Abstract Machine API", version = "1.0.0"),
    )(cam, lambda, camTask, submitScore, login, launch)

  override def found: Int = 302
}

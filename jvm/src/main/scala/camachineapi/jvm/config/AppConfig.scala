package camachineapi.jvm.config

import org.http4s.Uri
import pureconfig.generic.ProductHint
import pureconfig.{CamelCase, ConfigFieldMapping, ConfigReader}
import pureconfig.generic.semiauto._
import pureconfig.module.http4s.uriReader

case class Http(port: Int, host: String)

object Http {
  implicit val httpConfigReader: ConfigReader[Http] = deriveReader[Http]
}
case class MoodleConfig(
  url: Uri,
  pubKey: String,
  secKey: String,
  clientId: String,
)
object MoodleConfig extends CamelCaseProductHint {
  implicit val moodleConfigReader: ConfigReader[MoodleConfig] =
    deriveReader[MoodleConfig]
}

case class AppConfig(http: Http, assets: String, moodle: MoodleConfig)

object AppConfig extends CamelCaseProductHint {
  implicit val appConfigReader: ConfigReader[AppConfig] =
    deriveReader[AppConfig]
}

trait CamelCaseProductHint {
  implicit def productHint[T] =
    ProductHint[T](
      fieldMapping = ConfigFieldMapping(CamelCase, CamelCase),
    )
}

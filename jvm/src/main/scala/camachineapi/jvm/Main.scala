package camachineapi.jvm

import camachineapi.jvm.config.AppConfig
import camachineapi.jvm.http.CamApiDocs.api
import camachineapi.jvm.http._
import cats.data.OptionT
import cats.effect.{Blocker, ExitCode, IO, IOApp, Resource}
import cats.syntax.all._
import endpoints4s.openapi.model.OpenApi
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.implicits.http4sKleisliResponseSyntaxOptionT
import org.http4s.server.Server
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.{CORS, UrlFormLifter}
import pureconfig.ConfigSource
import pureconfig.module.catseffect2.syntax.CatsEffectConfigSource
import scribe.{Level, Logger}
import sttp.client3.http4s.Http4sBackend
import sttp.tapir.swagger.http4s.SwaggerHttp4s

import scala.concurrent.ExecutionContext

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    for {
      _ <- IO(
        Logger.root
          .clearHandlers().clearModifiers()
          .withHandler(minimumLevel = Some(Level.Debug))
          .replace(),
      )
      exitCode <- app.use(_ => IO.never).as(ExitCode.Success)
    } yield exitCode

  private def app: Resource[IO, Server[IO]] =
    for {
      blocker <- Blocker[IO]
      appConfig <- Resource.eval(
        ConfigSource.default.loadF[IO, AppConfig](blocker),
      )
      staticEndpoints = new StaticEndpoints[IO](appConfig.assets, blocker)
      docRoutes = new SwaggerHttp4s(
        OpenApi.stringEncoder.encode(api),
        yamlName = "docs.json",
      ).routes[IO]
      http <- BlazeClientBuilder[IO](
        ExecutionContext.global,
      ).resource
      sttpClient = Http4sBackend.usingClient(http, blocker)
      httpApp = UrlFormLifter(OptionT.liftK[IO])(
        staticEndpoints.endpoints() <+>
          new CamEndpoints[IO].endpoints() <+>
          new LambdaToCclEndpoints[IO].endpoints() <+>
          new LtiEndpoints[IO](appConfig.moodle, sttpClient).endpoints() <+>
          docRoutes,
      ).orNotFound
      server <- BlazeServerBuilder[IO](ExecutionContext.global)
        .bindHttp(appConfig.http.port, appConfig.http.host)
        .withHttpApp(CORS.policy(httpApp))
        .resource
    } yield server
}

package camachineapi

import camachineapi.CAMachine.Row
import camachineapi.models.{
  CamCompilerData,
  CamCompilerResult,
  LambdaCode,
  LambdaTask,
  LtiUserData,
  RecvarsDto,
  VariableDto,
}
import endpoints4s.algebra
import endpoints4s.algebra.Endpoints
import endpoints4s.generic.JsonSchemas

trait CamApiEndpoints
    extends Endpoints
    with algebra.JsonEntitiesFromSchemas
    with JsonSchemas {
  val submitScore: Endpoint[LtiUserData, Either[String, Unit]] =
    endpoint(
      post(path / "submitScore", jsonRequest[LtiUserData]),
      response(BadRequest, textResponse)
        .orElse(ok(emptyResponse)),
    )
  implicit lazy val ltiUserDataSchema: JsonSchema[LtiUserData] =
    genericJsonSchema[LtiUserData]
  val cam: Endpoint[CamCompilerData, CamCompilerResult[String]] =
    endpoint(
      post(path / "api" / "cam", jsonRequest[CamCompilerData]),
      ok(jsonResponse[CamCompilerResult[String]]),
    )
  val camTask: Endpoint[Unit, CamCompilerResult[Option[String]]] =
    endpoint(
      post(path / "api" / "camTask", emptyRequest),
      ok(jsonResponse[CamCompilerResult[Option[String]]]),
    )
  val lambda: Endpoint[LambdaCode, LambdaCode] =
    endpoint(
      post(path / "api" / "lambda", jsonRequest[LambdaCode]),
      ok(jsonResponse[LambdaCode]),
    )
  val lambdaTask: Endpoint[Unit, LambdaTask] =
    endpoint(
      post(path / "api" / "lambdaTask", emptyRequest),
      ok(jsonResponse[LambdaTask]),
    )

  implicit lazy val lambdaTaskSchema: JsonSchema[LambdaTask] =
    genericJsonSchema[LambdaTask]
  implicit lazy val recvarsSchema: JsonSchema[RecvarsDto] =
    genericJsonSchema[RecvarsDto]
  implicit lazy val varSchema: JsonSchema[VariableDto] =
    genericJsonSchema[VariableDto]
  implicit lazy val rowSchema: JsonSchema[Row[String]] =
    genericJsonSchema[Row[String]]
  implicit lazy val rowOptSchema: JsonSchema[Row[Option[String]]] =
    genericJsonSchema[Row[Option[String]]]
  implicit lazy val camDataSchema: JsonSchema[CamCompilerData] =
    genericJsonSchema[CamCompilerData]
  implicit lazy val camResultSchema: JsonSchema[CamCompilerResult[String]] =
    genericJsonSchema[CamCompilerResult[String]]
  implicit lazy val camTaskResultSchema
    : JsonSchema[CamCompilerResult[Option[String]]] =
    genericJsonSchema[CamCompilerResult[Option[String]]]
  implicit lazy val lambdaSchema: JsonSchema[LambdaCode] =
    genericJsonSchema[LambdaCode]

}

package camachineapi.models

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class LambdaCode(code: String) extends AnyVal

object LambdaCode {
  implicit val lambdaCodeEncoder: Encoder[LambdaCode] = deriveEncoder
  implicit val lambdaCodeDecoder: Decoder[LambdaCode] = deriveDecoder
}

case class LambdaTask(
  lambdaTerm: Option[String],
  cclTerm: Option[String],
  camCommand: Option[String],
)

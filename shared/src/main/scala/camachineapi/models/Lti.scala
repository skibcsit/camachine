package camachineapi.models

import io.circe.generic.JsonCodec
import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec}

import java.time.LocalDateTime

case class LtiLoginData(
  iss: String,
  targetLinkUri: String,
  loginHint: Int,
  ltiMessageHint: Int,
  clientId: String,
  ltiDeploymentId: Int,
)

case class LtiTokenData(
  idToken: String,
  state: String,
)

case class LtiUserData(
  contextId: Int,
  userId: Int,
)

@JsonCodec
case class LtiScoreData(
  activityProgress: String,
  comment: String,
  gradingProgress: String,
  scoreGiven: Int,
  scoreMaximum: Int,
  timestamp: LocalDateTime,
  userId: Int,
)

@ConfiguredJsonCodec
case class LtiTokenResponse(
  accessToken: String,
  tokenType: String,
  scope: String,
  expiresIn: Int,
)
object LtiTokenResponse {
  implicit private val jsonCodecConfig: Configuration =
    Configuration.default.withSnakeCaseMemberNames
}

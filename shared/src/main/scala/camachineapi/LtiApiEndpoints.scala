package camachineapi
import camachineapi.models.{LtiLoginData, LtiTokenData}
import endpoints4s.algebra.{Endpoints, Errors, Responses}

trait ErrorOrRedirectResponse extends Responses with Errors {

  def found: StatusCode

  def errorOrRedirect: Response[Either[String, String]] =
    choiceResponse(
      response(BadRequest, textResponse),
      response(found, emptyResponse, headers = responseHeader("Location")),
    )

}

trait LtiApiEndpoints extends Endpoints with ErrorOrRedirectResponse {

  def login: Endpoint[LtiLoginData, Either[String, String]] =
    endpoint(
      post(path / "login" /? ltiLoginDataQueryString, emptyRequest),
      errorOrRedirect,
    )

  def launch: Endpoint[LtiTokenData, Either[String, String]] =
    endpoint(
      post(path / "launch" /? ltiTokenDataQueryString, emptyRequest),
      errorOrRedirect,
    )

  val ltiTokenDataQueryString: QueryString[LtiTokenData] =
    (qs[String]("id_token") &
      qs[String]("state")).xmap {
      case (
            idToken,
            state,
          ) =>
        LtiTokenData(
          idToken,
          state,
        )
    } { ltiTokenData =>
      (
        ltiTokenData.idToken,
        ltiTokenData.state,
      )
    }
  val ltiLoginDataQueryString: QueryString[LtiLoginData] =
    (
      qs[String]("iss") &
        qs[String]("target_link_uri") &
        qs[Int]("login_hint") &
        qs[Int]("lti_message_hint") &
        qs[String]("client_id") & qs[Int]("lti_deployment_id")
    ).xmap {
      case (
            iss,
            targetLinkUri,
            loginHint,
            ltiMessageHint,
            clientId,
            ltiDeploymentId,
          ) =>
        LtiLoginData(
          iss,
          targetLinkUri,
          loginHint,
          ltiMessageHint,
          clientId,
          ltiDeploymentId,
        )
    } { ltiLoginData =>
      (
        ltiLoginData.iss,
        ltiLoginData.targetLinkUri,
        ltiLoginData.loginHint,
        ltiLoginData.ltiMessageHint,
        ltiLoginData.clientId,
        ltiLoginData.ltiDeploymentId,
      )
    }

}

package camachineapi.CAMachine

trait Element {
  override def toString: String =
    this match {
      case a: Atom => a.getValue
      case a: Pair =>
        var str: String = ""
        str += "("
        if (a.getFirst == null) str += "null"
        else str = str + a.getFirst.toString()
        str += ", "
        if (a.getSecond == null) str += "null"
        else str += a.getSecond.toString()
        str += ")"
        str
      case a: Function =>
        if (a.getElement == null) a.getFunc + ":" + "null"
        else a.getFunc + ":" + a.getElement.toString()
      case _ => ""
    }
}

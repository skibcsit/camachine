package camachineapi.js.models

import enumeratum._
import cats.syntax.option._

sealed trait ContextualColor extends EnumEntry {
  def title: Option[String]
}

object ContextualColor extends Enum[ContextualColor] {

  val values: IndexedSeq[ContextualColor] = findValues

  case object Primary extends ContextualColor {
    val title: Option[String] = None
  }
  case object Secondary extends ContextualColor {
    val title: Option[String] = None
  }
  case object Success extends ContextualColor {
    val title: Option[String] = "Успех".some
  }
  case object Danger extends ContextualColor {
    val title: Option[String] = "Ошибка".some
  }
  case object Warning extends ContextualColor {
    val title: Option[String] = "Внимание".some
  }
  case object Info extends ContextualColor {
    val title: Option[String] = None
  }
  case object Light extends ContextualColor {
    val title: Option[String] = None
  }
  case object Dark extends ContextualColor {
    val title: Option[String] = None
  }
  case object Muted extends ContextualColor {
    val title: Option[String] = None
  }
  case object White extends ContextualColor {
    val title: Option[String] = None
  }

}

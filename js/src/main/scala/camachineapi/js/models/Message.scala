package camachineapi.js.models

import cats.syntax.option._

final case class Message(color: ContextualColor, text: String)

object Message {
  def from(exception: Exception): Message =
    Message(ContextualColor.Danger, exception.getMessage)
}

trait HasMessage {
  def message: Message
}

object HasMessage {
  def unapply(arg: HasMessage): Option[Message] = arg.message.some
}

trait HasPrimaryMessage extends HasMessage {
  def messageText: String
  def message: Message = Message(ContextualColor.Primary, messageText)
}
trait HasSecondaryMessage extends HasMessage {
  def messageText: String
  def message: Message = Message(ContextualColor.Secondary, messageText)
}
trait HasSuccessMessage extends HasMessage {
  def messageText: String
  def message: Message = Message(ContextualColor.Success, messageText)
}
trait HasDangerMessage extends HasMessage {
  def messageText: String
  def message: Message = Message(ContextualColor.Danger, messageText)
}
trait HasWarningMessage extends HasMessage {
  def messageText: String
  def message: Message = Message(ContextualColor.Warning, messageText)
}
trait HasInfoMessage extends HasMessage {
  def messageText: String
  def message: Message = Message(ContextualColor.Info, messageText)
}
trait HasLightMessage extends HasMessage {
  def messageText: String
  def message: Message = Message(ContextualColor.Light, messageText)
}
trait HasDarkMessage extends HasMessage {
  def messageText: String
  def message: Message = Message(ContextualColor.Dark, messageText)
}
trait HasMutedMessage extends HasMessage {
  def messageText: String
  def message: Message = Message(ContextualColor.Muted, messageText)
}
trait HasWhiteMessage extends HasMessage {
  def messageText: String
  def message: Message = Message(ContextualColor.White, messageText)
}

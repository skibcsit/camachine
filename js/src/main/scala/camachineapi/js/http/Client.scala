package camachineapi.js.http

import camachineapi.models.{
  CamCompilerData,
  CamCompilerResult,
  LambdaCode,
  LtiUserData,
}
import cats.effect.{ContextShift, IO}

class Client(
  protocolHost: String,
) {

  def requestCam(
    data: CamCompilerData,
  )(implicit
    contextShift: ContextShift[IO],
  ): IO[CamCompilerResult[String]] =
    IO.fromFuture(IO(Api(protocolHost).cam(data).toFuture))

  def requestCamTask()(implicit
    contextShift: ContextShift[IO],
  ): IO[CamCompilerResult[Option[String]]] =
    IO.fromFuture(IO(Api(protocolHost).camTask(()).toFuture))
  def requestLambda(
    data: LambdaCode,
  )(implicit
    contextShift: ContextShift[IO],
  ): IO[LambdaCode] =
    IO.fromFuture(IO(Api(protocolHost).lambda(data).toFuture))

  def requestSubmitScore(
    data: LtiUserData,
  )(implicit
    contextShift: ContextShift[IO],
  ): IO[Either[String, Unit]] =
    IO.fromFuture(IO(Api(protocolHost).submitScore(data).toFuture))

}

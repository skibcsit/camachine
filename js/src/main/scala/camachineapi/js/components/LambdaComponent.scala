package camachineapi.js.components

import camachineapi.js.forms.Forms._
import camachineapi.js.http.Client
import camachineapi.models.LambdaCode
import cats.effect.{ContextShift, IO}
import monix.reactive.Observable
import outwatch.Handler
import outwatch.dom.VDomModifier
import outwatch.dom.dsl._

class LambdaComponent(
  textAreaLambda: Handler[String],
  resultLambda: Handler[LambdaCode],
  client: Client,
)(implicit
  contextShift: ContextShift[IO],
) {
  def dataStream: Observable[LambdaCode] =
    textAreaLambda.map(t => LambdaCode.apply(t.trim))

  def sendRequestIO(data: LambdaCode): IO[LambdaCode] =
    client.requestLambda(data)

  def disableStream: Observable[Boolean] = dataStream.map(_.code.length < 4)

  def node =
    div(
      textAreaGroupRow(
        textAreaLambda,
        "lambdaTextArea",
        "Лямбда код",
        "Введите последовательность лямбда-термов",
        isRequired = true,
      ),
      buttonSubmitGroupRow(
        "Отправить",
        resultLambda,
        disableStream,
        dataStream,
        sendRequestIO,
      ),
      VDomModifier(
        resultLambda.map(result =>
          VDomModifier(
            h3("Терм ККЛ полученый из лямбда-термов"),
            table(
              cls := "table table-bordered",
              thead(
                tr(
                  td(attr("scope") := "col", "Лямбда-терм"),
                  td(attr("scope") := "col", "ККЛ терм"),
                  td(attr("scope") := "col", "Команда КАМ"),
                ),
              ),
              tr(
                result.code.split("->").map(s => td(s)),
              ),
            ),
          ),
        ),
      ),
    )
}

object LambdaComponent {
  def init(
    client: Client,
  )(implicit
    contextShift: ContextShift[IO],
  ): IO[LambdaComponent] =
    for {
      textAreaLambdaHandler <- Handler.create[String]
      resultLambdaHandler   <- Handler.create[LambdaCode]
    } yield
      new LambdaComponent(
        textAreaLambdaHandler,
        resultLambdaHandler,
        client,
      )
}

package camachineapi.js.components

import camachineapi.js.models.Message
import outwatch.dom.BasicVNode
import outwatch.dom.dsl._

object FlashComponent {

  def flash(message: Message): BasicVNode =
    div(
      cls :=
        s"alert alert-${message.color.entryName.toLowerCase} " +
          s"alert-dismissible fade show mt-2",
      role := "alert",
      message.color.title.map(text => strong(s"$text:")),
      " ",
      span(onDomMount.foreach(_.innerHTML = message.text)),
      button(
        `type` := "button",
        cls := "close",
        attr("data-dismiss") := "alert",
        attr("aria-label") := "Закрыть",
        span(attr("aria-hidden") := "true", "×"),
      ),
    )

}

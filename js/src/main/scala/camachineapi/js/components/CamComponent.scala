package camachineapi.js.components

import camachineapi.CAMachine.Row
import camachineapi.js.UIComponent
import camachineapi.js.forms.Forms._
import camachineapi.js.http.Client
import camachineapi.js.models.{
  HasDangerMessage,
  HasMessage,
  HasSuccessMessage,
  Message,
}
import camachineapi.models.{CamCompilerData, CamCompilerResult}
import cats.effect.{ContextShift, IO}
import cats.syntax.semigroup._
import monix.execution.Ack.{Continue, Stop}
import mouse.boolean._
import monix.reactive.Observable
import org.scalajs.dom.ext.AjaxException
import outwatch.Handler
import outwatch.dom.dsl._
import outwatch.dom.{BasicVNode, VDomModifier}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class CamComponent(
  textAreaCam: Handler[String],
  textAreaCamCodeTask: Handler[String],
  textAreaCamTermTask: Handler[String],
  textAreaCamStackTask: Handler[String],
  radioInputCam: Handler[String],
  resultCam: Handler[CamCompilerResult[String]],
  eventHandler: Handler[CamComponent.Event],
  client: Client,
)(implicit
  contextShift: ContextShift[IO],
) extends UIComponent {
  def choice(
    answer: Handler[String],
    options: List[String],
  ): Seq[BasicVNode] =
    options.map(option =>
      radioGroupRow(answer, "opt" + option, option, "optimization"),
    )

  def convertRadioToInt(value: String) = value match {
    case "Без оптимизации"                        => 0
    case "Бета-свертывание"                       => 1
    case "Двухместные функции"                    => 2
    case "Бета-свертывание + Двухместные функции" => 3
    case _                                        => 0
  }
  def dataStream: Observable[CamCompilerData] =
    Observable
      .combineLatestMap2(radioInputCam, textAreaCam)((r, t) =>
        CamCompilerData.apply(convertRadioToInt(r), t.trim),
      )

  def messageStream: Observable[Message] =
    eventHandler.collect { case HasMessage(message) => message }

  def flashStream: Observable[VDomModifier] =
    messageStream
      .scan(VDomModifier.empty)((mod, message) =>
        mod.combine(FlashComponent.flash(message)),
      )
  private def generateTaskStream =
    eventHandler.collect { case e @ CamComponent.GenerateTaskEvent =>
      e
    }.flatMap(_ =>
      Observable.from(
        for {
          taskResult <- client.requestCamTask()
          fullResult <- client.requestCam(
            CamCompilerData(0, taskResult.rows.head.code.getOrElse("")),
          )
        } yield (taskResult, fullResult),
      ),
    )

  private def dataRowStream: Observable[Row[String]] =
    Observable
      .combineLatestMap3(
        textAreaCamCodeTask,
        textAreaCamTermTask,
        textAreaCamStackTask,
      )(
        (
          camCode,
          camTerm,
          camStack,
        ) =>
          Row(
            camTerm,
            camCode,
            camStack,
          ),
      )
  def sendRequestIO(data: CamCompilerData): IO[CamCompilerResult[String]] =
    client
      .requestCam(data).attempt.flatMap(
        _.fold(
          {
            case e: AjaxException =>
              IO.fromFuture(
                IO(
                  eventHandler
                    .onNext(
                      CamComponent.ErrorEvent(e.xhr.responseText),
                    ).flatMap {
                      case Continue =>
                        Future.successful(
                          CamComponent.ErrorEvent(e.xhr.responseText),
                        )
                      case Stop =>
                        Future.failed(new Exception("Failed! Ack is Stop"))
                    },
                ),
              )

            case error =>
              IO.fromFuture(
                IO(
                  eventHandler
                    .onNext(CamComponent.ErrorEvent(error.getMessage)).flatMap {
                      case Continue =>
                        Future
                          .successful(CamComponent.ErrorEvent(error.getMessage))
                      case Stop =>
                        Future.failed(new Exception("Failed! Ack is Stop"))
                    },
                ),
              )
          },
          data =>
            IO.fromFuture(
              IO(eventHandler.onNext(CamComponent.DataEvent(data)).flatMap {
                case Continue =>
                  Future.successful(CamComponent.DataEvent(data))
                case Stop =>
                  Future.failed(new Exception("Failed! Ack is Stop"))
              }),
            ),
        ),
      ).map {
        case CamComponent.DataEvent(data) => data
        case CamComponent.ErrorEvent(error) =>
          CamCompilerResult(
            List.empty,
            List.empty,
            List(Row("-", s"Error: $error", "-")),
            error,
          )
      }

  def disableStream: Observable[Boolean] = dataStream.map(_.commands.length < 4)

  def node: BasicVNode = div(
    div(
      cls := "position-sticky",
      styleAttr := "top:5px;z-index:2",
      VDomModifier(flashStream),
    ),
    div(
      cls := "form-group",
      p(
        choice(
          radioInputCam,
          List(
            "Без оптимизации",
            "Бета-свертывание",
            "Двухместные функции",
            "Бета-свертывание + Двухместные функции",
          ),
        ),
      ),
    ),
    textAreaGroupRow(
      textAreaCam,
      "camTextArea",
      "КАМ код",
      "Введите последовательность команд КАМ через пробел",
      isRequired = true,
    ),
    buttonSubmitGroupRow(
      "Отправить",
      resultCam,
      disableStream,
      dataStream,
      sendRequestIO,
    ),
    VDomModifier(
      resultCam.map(result =>
        VDomModifier(
          VDomModifier(
            h3("Используемые переменные(рекурсивные)"),
            table(
              cls := "table table-bordered",
              result.recvars.map(recvar =>
                tr(
                  td(
                    recvar.name,
                  ),
                  td(
                    recvar.value,
                  ),
                ),
              ),
            ),
          ),
          VDomModifier(
            h3("Используемые переменные"),
            table(
              cls := "table table-bordered",
              result.variables.map(variable =>
                tr(
                  td(
                    variable.name,
                  ),
                  td(
                    variable.value,
                  ),
                ),
              ),
            ),
          ),
          VDomModifier(
            h3("Состояния КАМ на каждом шагу выполнения"),
            table(
              cls := "table table-bordered",
              thead(
                tr(
                  td(attr("scope") := "col", "Терм"),
                  td(attr("scope") := "col", "Код"),
                  td(attr("scope") := "col", "Стек"),
                ),
              ),
              result.rows.map(row =>
                tr(
                  td(
                    styleAttr := "bgcolor=#b9e4c9",
                    row.term,
                  ),
                  td(
                    styleAttr := "bgcolor=#fedbd0",
                    row.code,
                  ),
                  td(
                    styleAttr := "bgcolor=#b9e4c9",
                    row.stack,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    div(
      cls := "form-group row",
      div(
        cls := "col-sm-9",
        button(
          `type` := "submit",
          cls := "btn btn-success",
          "Сгенерировать задание",
          onClick
            .preventDefault(CamComponent.GenerateTaskEvent)
            --> eventHandler,
        ),
      ),
    ),
    VDomModifier(
      generateTaskStream.map(data =>
        VDomModifier(
          VDomModifier(
            h3("Используемые переменные(рекурсивные)"),
            table(
              cls := "table table-bordered",
              data._1.recvars.map(recvar =>
                tr(
                  td(
                    recvar.name,
                  ),
                  td(
                    recvar.value,
                  ),
                ),
              ),
            ),
          ),
          VDomModifier(
            h3("Используемые переменные"),
            table(
              cls := "table table-bordered",
              data._1.variables.map(variable =>
                tr(
                  td(
                    variable.name,
                  ),
                  td(
                    variable.value,
                  ),
                ),
              ),
            ),
          ),
          h3("Состояния КАМ на каждом шагу выполнения"),
          table(
            cls := "table table-bordered",
            thead(
              tr(
                td(attr("scope") := "col", "Терм"),
                td(attr("scope") := "col", "Код"),
                td(attr("scope") := "col", "Стек"),
              ),
            ),
            data._1.rows.map(row =>
              VDomModifier(
                tr(
                  td(
                    styleAttr := "bgcolor=#b9e4c9",
                    row.term.fold(
                      VDomModifier(
                        textAreaGroupRow(
                          textAreaCamTermTask,
                          "camTaskTextArea",
                          "Терм",
                          "Введите значение терма",
                          isRequired = true,
                        ),
                      ),
                    )(VDomModifier(_)),
                  ),
                  td(
                    styleAttr := "bgcolor=#fedbd0",
                    row.code.fold(
                      VDomModifier(
                        textAreaGroupRow(
                          textAreaCamCodeTask,
                          "camTaskTextArea",
                          "КАМ код",
                          "Введите команды, которые были в этом состоянии КАМ",
                          isRequired = true,
                        ),
                      ),
                    )(VDomModifier(_)),
                  ),
                  td(
                    styleAttr := "bgcolor=#b9e4c9",
                    row.stack.fold(
                      VDomModifier(
                        textAreaGroupRow(
                          textAreaCamStackTask,
                          "camTaskTextArea",
                          "Стек",
                          "Введите значение стека",
                          isRequired = true,
                        ),
                      ),
                    )(VDomModifier(_)),
                  ),
                ),
              ),
            ),
          ),
          div(
            cls := "form-group row",
            div(
              cls := "col-sm-9",
              button(
                `type` := "submit",
                cls := "btn btn-success",
                "Проверить",
                onClick
                  .preventDefault(dataRowStream)
                  .transform(
                    _.flatMap(row =>
                      Observable
                        .from(
                          IO.pure(
                            data._2.rows
                              .exists(dataRow =>
                                Row.ignoreCaseEquals(dataRow, row),
                              ).fold(
                                t = CamComponent.DoneEvent,
                                f = CamComponent.ErrorEvent(
                                  "Состояние КАМ восстановлено неверно!",
                                ),
                              ),
                          ),
                        ),
                    ),
                  )
                  --> eventHandler,
              ),
            ),
          ),
        ),
      ),
    ),
    br,
  )
}

object CamComponent {
  sealed trait Event

  object GenerateTaskEvent extends Event
  final case class ErrorEvent(messageText: String)
      extends Event
      with HasDangerMessage
  object DoneEvent extends Event with HasSuccessMessage {
    val messageText = "Состояние КАМ правильно восстановлено!"
  }
  final case class DataEvent(data: CamCompilerResult[String]) extends Event

  def init(
    client: Client,
  )(implicit
    contextShift: ContextShift[IO],
  ): IO[CamComponent] =
    for {
      textAreaCamHandler          <- Handler.create[String]
      textAreaCamCodeTaskHandler  <- Handler.create[String]
      textAreaCamTermTaskHandler  <- Handler.create[String]
      textAreaCamStackTaskHandler <- Handler.create[String]
      radioInputCamHandler        <- Handler.create[String]
      resultCamHandler            <- Handler.create[CamCompilerResult[String]]
      eventHandler                <- Handler.create[Event]
    } yield
      new CamComponent(
        textAreaCamHandler,
        textAreaCamCodeTaskHandler,
        textAreaCamTermTaskHandler,
        textAreaCamStackTaskHandler,
        radioInputCamHandler,
        resultCamHandler,
        eventHandler,
        client,
      )
}

package camachineapi.js

import camachineapi.js.components.{
  CamComponent,
  CamLtiComponent,
  LambdaComponent,
}
import camachineapi.js.http.Client
import cats.effect.{ContextShift, IO}
import org.scalajs.dom.window
import outwatch.dom.BasicVNode
import outwatch.dom.dsl._

trait UIComponent {
  def node: BasicVNode
}

final case class TopComponent(
  camComponent: UIComponent,
  lambdaComponent: LambdaComponent,
) {

  def node: BasicVNode =
    div(
      header(
        div(
          cls := "wrapper",
          div(
            cls := "jumbotron text-center",
            styleAttr := "margin-bottom:0",
            h1("Categorical Abstract Machine"),
          ),
        ),
      ),
      div(
        cls := "container",
        camComponent.node,
      ),
      div(
        cls := "container",
        lambdaComponent.node,
      ),
      footer(
        div(
          cls := "wrapper",
          div(
            cls := "jumbotron text-center",
            styleAttr := "margin-bottom:0",
            p("НИЯУ МИФИ 2022"),
          ),
        ),
      ),
    )

}

object TopComponent {

  def init(client: Client)(implicit
    contextShift: ContextShift[IO],
  ): IO[TopComponent] =
    for {
      maybeRegexLtiMatch <- IO(
        """(\d+)/(\d+)/(.+)""".r.findFirstMatchIn(window.location.hash),
      )
      lambdaComponent <- LambdaComponent.init(client)
      camComponent <- maybeRegexLtiMatch match {
        case Some(regexLtiMatch) =>
          CamLtiComponent.init(
            client,
            regexLtiMatch.group(1).toInt,
            regexLtiMatch.group(2).toInt,
            regexLtiMatch.group(3),
          )
        case _ => CamComponent.init(client)
      }
    } yield TopComponent(camComponent, lambdaComponent)

}
